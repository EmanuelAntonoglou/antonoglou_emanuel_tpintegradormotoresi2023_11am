using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;

[CreateAssetMenu(fileName = "Nueva Arma", menuName = "Arma")]
public class Arma : ScriptableObject
{
    public bool spread = true;
    public Vector3 varianteSpread = new Vector3(0.1f, 0.1f, 0.1f);
    public GameObject bala;
    public float delay = 0.3f;
    public float velocidad = 100;
    public LayerMask capa;

    [NonSerialized] public bool activa = false;
    [NonSerialized] public Transform origen;
    [NonSerialized] public float tiempoUltimoDisparo = 0;
    [NonSerialized] public TrailRenderer trailBala;
}
