using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Nueva Habilidad", menuName = "Habilidad")]
public class Habilidad : ScriptableObject
{
    public Sprite SkillImg;
    public Sprite KeyImg;
    public float velocidad;
    public float duracion;
    public float cooldown;

    [NonSerialized] public bool uso;
    [NonSerialized] public bool puedeUsar;
    [NonSerialized] public Cronometro cooldownSkill;
    [NonSerialized] public Cronometro duracionSkill;

    public void OnEnable()
    {
        uso = false;
        puedeUsar = true;
        cooldownSkill = new Cronometro(cooldown);
        duracionSkill = new Cronometro(duracion);
    }
}
