using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlVuelo : MonoBehaviour
{
    [SerializeField] private float velocidadVuelo = 50f;
    [SerializeField] private LayerMask capaParedes;
    [SerializeField] private float distanciaDeteccion = 10f;
    [SerializeField] private float distanciaEstacionar = 5f;
    [SerializeField] private float distanciaAdelante = 15f;
    [SerializeField] private Habilidad habilidadRayo;
    [SerializeField] private Habilidad habilidadDash;

    private Camera camara;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        camara = Camera.main;
    }

    private void Update()
    {
        Estacionar();
    }

    void FixedUpdate()
    {
        Volar();
    }

    private void Volar()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 direccionCamara = camara.transform.forward;

            Ray rayo = new Ray(transform.position, direccionCamara);
            RaycastHit hit;

            if (Physics.Raycast(rayo, out hit, distanciaDeteccion, capaParedes))
            {
                return;
            }

            rb.MovePosition(rb.position + direccionCamara * velocidadVuelo * Time.fixedDeltaTime);
        }
    }

    private void Estacionar()
    {
        Ray rayoHaciaAbajo = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        int capaSuelo = LayerMask.NameToLayer("Suelo");
        Ray rayoHaciaAdelante = new Ray(transform.position, transform.forward);
        RaycastHit hitAdelante;
        int capaAdelante = LayerMask.NameToLayer("Habitacion");

        if (Physics.Raycast(rayoHaciaAbajo, out hit, distanciaEstacionar, 1 << capaSuelo)
            && !GameManager.i.habilidadesActuales[1].uso)
        {
            bool raycastHaciaAdelanteExitoso = Physics.Raycast(rayoHaciaAdelante, out hitAdelante, distanciaAdelante, 1 << capaAdelante);
            if (!raycastHaciaAdelanteExitoso || (raycastHaciaAdelanteExitoso && hitAdelante.collider.gameObject.layer != capaAdelante && hitAdelante.distance > distanciaEstacionar))
            {
                UIManager.i.CambiarFillAmountHabilidad(2, 0);

                if (Input.GetKeyDown(KeyCode.LeftControl))
                {
                    rb.velocity = Vector3.zero;
                    rb.useGravity = true;

                    ControlAlfombra alfombra = GetComponentInChildren<ControlAlfombra>();
                    alfombra.activo = false;
                    alfombra.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                    alfombra.col.enabled = true;
                    alfombra.transform.SetParent(null);

                    transform.position += transform.forward * 10f;
                    ControlVuelo controlVuelo = GetComponent<ControlVuelo>();
                    ControlPersonaje componenteActual = GetComponent<ControlPersonaje>();
                    rb.velocity = Vector3.zero;
                    componenteActual.enabled = true;
                    controlVuelo.enabled = false;


                    GameManager.i.CambiarHabilidad(1, GameManager.i.habilidadesAnteriores[1]);
                    GameManager.i.CambiarHabilidad(2, GameManager.i.habilidadesAnteriores[2]);
                    UIManager.i.CargarHabilidades();
                    if (GameManager.i.personaje.poderAgotado)
                    {
                        UIManager.i.CambiarFillAmountHabilidad(1, 1);
                    }

                    if (GameManager.i.habitacionActual.arena)
                    {
                        Transform alfombraTransform = GameManager.i.personaje.alfombra.transform;
                        Transform arenaTransform = GameManager.i.habitacionActual.arena.transform;
                        alfombraTransform.SetParent(arenaTransform);
                    }
                }
            }
            else
            {
                UIManager.i.CambiarFillAmountHabilidad(2, 1);
            }
        }
        else
        {
            UIManager.i.CambiarFillAmountHabilidad(2, 1);
        }

    }


}

