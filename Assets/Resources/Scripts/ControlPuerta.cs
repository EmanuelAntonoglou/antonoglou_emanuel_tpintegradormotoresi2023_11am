using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;

public class ControlPuerta : MonoBehaviour
{
    public float tiempoParaSpawnear;
    public Cronometro tiempoSpawn;
    public ControlHabitacion habitacionSiguiente;

    [NonSerialized] public GameObject puertaCerrada;
    private GameObject nuevoEnemigo;
    private Material material;
    private MeshRenderer rend;
    public Color colorTitilar = Color.white;
    private Color colorNormal = Color.black;
    private bool puedeSalir = true;

    void Awake()
    {
        puertaCerrada = transform.Find("Puerta Cerrada").gameObject;
        rend = puertaCerrada.GetComponent<MeshRenderer>();
        material = rend.material;
        tiempoSpawn = new Cronometro(tiempoParaSpawnear);
        material.SetColor("_Color", colorNormal);
    }

    void Update()
    {
        EnemigoSaliendo();
    }

    public IEnumerator TitilarLuces(int titilaciones, float tiempoTitilar)
    {
        AudioSource[] audioSources = AudioManager.i.GetComponents<AudioSource>();
        AudioSource targetAudioSource = Array.Find(audioSources, source => source.clip.name == "Beep");

        for (int i = 0; i < titilaciones; i++)
        {
            if (targetAudioSource != null && !targetAudioSource.isPlaying)
            {
                AudioManager.i.ReproducirSonido("Beep");
            }
            material.SetColor("_Color", colorTitilar);
            yield return new WaitForSeconds(tiempoTitilar);
            material.SetColor("_Color", colorNormal);
            yield return new WaitForSeconds(tiempoTitilar);
        }
        AudioManager.i.ReproducirSonido("Plom");
    }
    
    public IEnumerator SpawnEnemigo(SpawnInfo spawn)
    {
        yield return tiempoSpawn.Tick(tiempoParaSpawnear, 0, false, 0);
        
        Vector3 direccionHaciaJugador = GameManager.i.personaje.transform.position - rend.bounds.center;
        direccionHaciaJugador.y = 0f;
        direccionHaciaJugador.Normalize();
        Quaternion rotacionHaciaAdelante = Quaternion.LookRotation(direccionHaciaJugador, Vector3.up);
        Quaternion rotacionFinal = Quaternion.Euler(0, rotacionHaciaAdelante.eulerAngles.y, 0);
        
        nuevoEnemigo = Instantiate(spawn.enemyPrefab, rend.bounds.center, rotacionFinal);
        CambiarValoresEnemigo(spawn, nuevoEnemigo);
        puedeSalir = false;
        ControlEnemigo controlEnemigo = nuevoEnemigo.GetComponent<ControlEnemigo>();
        TirarObjeto tirarObjeto = nuevoEnemigo.GetComponent<TirarObjeto>();
        if (controlEnemigo != null)
        {
            controlEnemigo.MoverAdelante(puertaCerrada.transform);
        }
        if (tirarObjeto != null)
        {
            Collider colliderPuerta = puertaCerrada.GetComponent<Collider>();
            tirarObjeto.LanzarParabola(puertaCerrada.transform);
        }
    }

    private void CambiarValoresEnemigo(SpawnInfo spawn, GameObject nuevoEnemigo)
    {
        ControlEnemigo enemigo = nuevoEnemigo.GetComponent<ControlEnemigo>();

        if (enemigo != null)
        {
            enemigo.velMov += spawn.statsEnemigo.velMov;
            enemigo.velRot += spawn.statsEnemigo.velRot;
            enemigo.tiempoSalida += spawn.statsEnemigo.tiempoSalida;
            enemigo.da�o += spawn.statsEnemigo.da�o;
            enemigo.vida += spawn.statsEnemigo.vida;
        }
    }

    public void CambiarColor(GameObject objetoASpawnear)
    {
        if (objetoASpawnear.tag == "Enemigo")
        {
            colorTitilar = Color.red;
        }
        else if (objetoASpawnear.tag == "Healing")
        {
            colorTitilar = Color.green;
        }
        else if (objetoASpawnear.tag == "Arma")
        {
            colorTitilar = Color.yellow;
        }
    }

    private void EnemigoSaliendo()
    {
        if (!puedeSalir)
        {
            if (!nuevoEnemigo.GetComponent<Salida>().salir.Alarma())
            {
                material.SetColor("_Color", colorTitilar);
            }
            else
            {
                material.SetColor("_Color", colorNormal);
                puedeSalir = true;
            }
        }
    }
}