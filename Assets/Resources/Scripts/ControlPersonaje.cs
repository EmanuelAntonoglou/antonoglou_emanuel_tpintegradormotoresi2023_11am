using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlPersonaje : MonoBehaviour
{
    [Header("Deteccion Colisiones")]
    public LayerMask capaPiso;

    [Header("Movimiento")]
    public float velMov;
    private float horizontalInput, verticalInput;
    private Vector3 movimiento, inputDireccion;

    [Header("Salto")] 
    public float magnitudSalto;
    public float gravedad;
    private bool dobleSalto;

    [Header("Dash")]
    private Habilidad dash;

    [Header("Rayo Laser")]
    public GameObject habilidadRayo;
    public Animator animRayo;
    public float tiempoMinimoAlargar = 1f;
    [NonSerialized] public bool poderAgotado = false;
    [NonSerialized] public bool gastarRayo = false;
    [NonSerialized] public bool rayoAlargando = false;
    public Habilidad rayo;
    private float tiempoInicioAlargar = 0f;
    public float poder;

    [Header("Arma")]
    public Arma armaActual;
    private Habilidad disparo;

    [Header("Alfombra")]
    public GameObject alfombra;

    [Header("Componentes")]
    private Rigidbody rb;
    private CapsuleCollider col;
    private ControlCamara direccionCamara;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        dobleSalto = true;
        disparo = GameManager.i.habilidadesActuales[0];
        
        dash = GameManager.i.habilidadesActuales[2];
        direccionCamara = transform.Find("Main Camera").GetComponent<ControlCamara>();
        transform.Find("Main Camera").Find("Arma").gameObject.GetComponent<ControlDisparo>().CambiarArma(armaActual, true);
    }
    
    private void Update()
    {
        ReiniciarVelocidad();
        AplicarGravedad();
        if (!dobleSalto)
        {
            dobleSalto = EstaEnPiso();
        }
        Saltar();
        Dash();
        RayoLaser();
    }

    void FixedUpdate()
    {
        MovimientoPersonaje();
    }

    private void MovimientoPersonaje()
    {
        if (!dash.uso)
        {
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");
            inputDireccion = new Vector3(horizontalInput, 0, verticalInput);
            inputDireccion.Normalize();
            movimiento = transform.TransformDirection(inputDireccion) * velMov;
            rb.AddForce(movimiento, ForceMode.Impulse);
        }
    }

    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && dobleSalto)
        {
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
            rb.AddForce(Vector3.up * magnitudSalto * gravedad, ForceMode.Impulse);
            if (!EstaEnPiso())
            {
                dobleSalto = false;
            }
        }
    }

    private void Dash()
    {
        if (dash.duracionSkill.Alarma())
        {
            rb.velocity = rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
            dash.duracionSkill.tiempoRestante = 0;
            dash.uso = false;
        }

        if (dash.cooldownSkill.Alarma())
        {
            dash.puedeUsar = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && !dash.uso && dash.puedeUsar)
        {
            dash.uso = true;
            dash.puedeUsar = false;
            StartCoroutine(dash.duracionSkill.Tick(dash.duracion, 1, false, 2));
            StartCoroutine(dash.cooldownSkill.Tick(dash.cooldown, 1, true, 2));
            rb.velocity = Vector3.zero;
            Vector3 direccionInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
            direccionInput.Normalize();
            Vector3 direccionDash = Quaternion.LookRotation(direccionCamara.transform.forward) * direccionInput;
            direccionDash.y = 0f;
            rb.AddForce(direccionDash * dash.velocidad, ForceMode.Impulse);
            transform.SetPositionAndRotation(transform.position, transform.rotation);
        }
    }

    private void RayoLaser()
    {
        if (rayo != null)
        {
            AnimatorStateInfo estadoAnimacion = animRayo.GetCurrentAnimatorStateInfo(0);

            if (Input.GetMouseButtonDown(1))
            {
                if (!poderAgotado && poder > 0)
                {
                    alfombra.GetComponent<Collider>().enabled = false;
                    habilidadRayo.SetActive(true);
                    tiempoInicioAlargar = Time.time;
                    rayoAlargando = true;
                }
            }

            if (rayoAlargando && Input.GetMouseButton(1))
            {
                float tiempoTranscurrido = Time.time - tiempoInicioAlargar;

                if (tiempoTranscurrido >= tiempoMinimoAlargar && poder > 0)
                {
                    if (AnimacionEnCurso(animRayo))
                    {
                        animRayo.SetBool("RayoAlargar", true);
                        animRayo.SetBool("RayoAcortar", false);
                    }

                    if (estadoAnimacion.IsName("RayoAlargar"))
                    {
                        gastarRayo = true;
                    }
                    if (gastarRayo)
                    {
                        poder -= Time.deltaTime;
                    }
                    if (poder <= 0)
                    {
                        AcortarRayo();
                        poderAgotado = true;
                        UIManager.i.CambiarFillAmountHabilidad(1, 1);
                    }
                }
                UIManager.i.CambiarVida(UIManager.i.habilidades[1].GetComponentInChildren<Slider>(), poder);
            }

            if (Input.GetMouseButtonUp(1))
            {
                AcortarRayo();
            }

            if (!rayoAlargando)
            {
                UIManager.i.CambiarFillAmountHabilidad(0, 0);
                if (estadoAnimacion.IsName("RayoInicio"))
                {
                    habilidadRayo.SetActive(false);
                }
                if (rayo != null)
                {
                    CargarRayo();
                    if (poder >= rayo.duracion)
                    {
                        poderAgotado = false;
                        UIManager.i.CambiarFillAmountHabilidad(1, 0);
                    }
                }
            }
            else
            {
                UIManager.i.CambiarFillAmountHabilidad(0, 1);
            }
        }
    }

    private bool AnimacionEnCurso(Animator animator)
    {
        if (animator != null && animator.isActiveAndEnabled)
        {
            return true;
        }
        return false;
    }

    public void AcortarRayo()
    {
        alfombra.GetComponent<Collider>().enabled = true;
        rayoAlargando = false;
        gastarRayo = false;
        if (AnimacionEnCurso(animRayo))
        {
            animRayo.SetBool("RayoAcortar", true);
            animRayo.SetBool("RayoAlargar", false);
        }
    }

    public void CargarRayo()
    {
        if (rayo != null && poder <= rayo.duracion)
        {
            poder = Mathf.Min(poder + Time.deltaTime, rayo.duracion);
            if (GameManager.i.habilidadesActuales[1].name == "Rayo")
            {
                UIManager.i.CambiarVida(UIManager.i.habilidades[1].GetComponentInChildren<Slider>(), poder);
            }
        }
    }

    public void SubirseAlfombra()
    {
        ControlVuelo controlVuelo = GetComponent<ControlVuelo>();
        ControlPersonaje componenteActual = GetComponent<ControlPersonaje>();
        rb.velocity = Vector3.zero;
        componenteActual.enabled = false;
        controlVuelo.enabled = true;
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, 
               col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void AplicarGravedad()
    {
        rb.AddForce(Vector3.down * gravedad, ForceMode.Force);
    }

    private void ReiniciarVelocidad()
    {
        if (!Input.anyKey)
        {
            rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Arma"))
        {
            Arma nuevaArma = collision.gameObject.GetComponent<ControlDisparo>().arma;
            transform.Find("Main Camera").Find("Arma").gameObject.GetComponent<ControlDisparo>().CambiarArma(nuevaArma, true);
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Healing"))
        {
            GameManager.i.CambiarVida("Curar", collision.gameObject.GetComponent<Healing>().healing);
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Moneda"))
        {
            GameManager.i.monedas += 10;
            UIManager.i.CambiarTMP(UIManager.i.monedasTMP, GameManager.i.monedas.ToString());
            AudioManager.i.ReproducirSonido("Moneda");
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Techo"))
        {
            GameManager.i.CambiarVida("Da�ar", 100);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SiguienteHabitacion") && GameManager.i.enemigosRestantes == 0)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Ignorar Trigger"))
            {
                return;
            }
            other.gameObject.SetActive(false);
            if (other.gameObject.GetComponentInParent<ControlPuerta>().habitacionSiguiente.spawn.Count != 0)
            {
                GameManager.i.CambiarHabitacion(other.gameObject.GetComponentInParent<ControlPuerta>().habitacionSiguiente);
                GameManager.i.habitacionGanada = false;
            }
        }
        if (other.gameObject.CompareTag("PuertaGanar"))
        {
            GameManager.i.Pauso();
            SceneManager.LoadScene("MainMenu");
        }
        if (other.gameObject.CompareTag("BalaEnemigo"))
        {
            GameManager.i.CambiarVida("Da�ar", 10);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            GameManager.i.CambiarVida("Da�ar", collision.gameObject.GetComponent<ControlEnemigo>().da�o);
        }
    }

}
