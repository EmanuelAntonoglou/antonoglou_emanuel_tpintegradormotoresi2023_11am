using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public float sensibilidad;
    public float suavizado;
    public bool activo = true;

    public Vector2 mouseMirar, suavidadV;

    private GameObject jugador;

    void Start()
    {
        Cursor.visible = false;
        jugador = transform.parent.gameObject;
    }

    void Update()
    {
        if (!GameManager.i.pauso && activo)
        {
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));
            suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
            suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

            mouseMirar += suavidadV;
            mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);

            transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
            jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up);
        }
    }
}