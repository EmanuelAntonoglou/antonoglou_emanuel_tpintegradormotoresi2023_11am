using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashEnemigo : MonoBehaviour
{
    public float velocidadDash = 30;
    public float tiempoEntreDash = 3;
    private ControlEnemigo controlEnemigo;
    private bool invocoDash = false;

    private void Start()
    {
        controlEnemigo = GetComponent<ControlEnemigo>();
    }

    private void Update()
    {
        if (!invocoDash && controlEnemigo.salio)
        {
            InvokeRepeating("DashAleatorio", 0f, tiempoEntreDash);
            invocoDash = true;
        }
    }

    private void DashAleatorio()
    {
        Vector3 direccionAleatoria = Random.onUnitSphere;
        direccionAleatoria.y = 0f;
        Dash(direccionAleatoria);
    }

    private void Dash(Vector3 direccion)
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        rb.velocity = direccion * velocidadDash;
    }

}
