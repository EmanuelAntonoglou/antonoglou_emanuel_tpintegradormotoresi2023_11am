using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting.Antlr3.Runtime;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AudioManager : MonoBehaviour
{
    public static AudioManager i;
    public List<Sonido> sonidos;

    void Awake()
    {
        CrearSingleton();
}

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sonido s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.loop = s.repetir;
        }
    }

    public void CargarAudios()
    {
        string folderPath = "Assets/Resources/Sounds";

#if UNITY_EDITOR
        string[] audioGuids = AssetDatabase.FindAssets("t:AudioClip", new[] { folderPath });

        foreach (var audioGuid in audioGuids)
        {
            string audioPath = AssetDatabase.GUIDToAssetPath(audioGuid);
            AudioClip audioClip = AssetDatabase.LoadAssetAtPath<AudioClip>(audioPath);
            Sonido nuevoSonido = new Sonido();
            nuevoSonido.Nombre = audioClip.name;
            nuevoSonido.ClipSonido = audioClip;
            nuevoSonido.Volumen = 0.5f;
            nuevoSonido.pitch = 1;
            nuevoSonido.repetir = false;
            if (!sonidos.Any(s => s.Nombre == nuevoSonido.Nombre))
            {
                sonidos.Add(nuevoSonido);
            }
        }
#endif
        AudioClip[] audioClips = Resources.LoadAll<AudioClip>("Sounds");

        foreach (var audioClip in audioClips)
        {
            Sonido nuevoSonido = new Sonido();
            nuevoSonido.Nombre = audioClip.name;
            nuevoSonido.ClipSonido = audioClip;
            nuevoSonido.Volumen = 0.5f;
            nuevoSonido.pitch = 1;
            nuevoSonido.repetir = false;
            if (!sonidos.Any(s => s.Nombre == nuevoSonido.Nombre))
            {
                sonidos.Add(nuevoSonido);
            }
        }
    }

    public void ReproducirSonido(string nombre)
    {
        Sonido s = sonidos.Find(sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
        }
    }

    public void PausarSonido(string nombre)
    {
        Sonido s = sonidos.Find(sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
        }
    }
    
    public void PausarSonidosActivos(bool pausar)
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();

        foreach (AudioSource audioSource in audioSources)
        {
            if (pausar)
            {
                audioSource.Pause();
            }
            else
            {
                audioSource.UnPause();
            }
        }
    }


}

