using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Propiedades
    [NonSerialized] public static UIManager i;

    [Header("Habilidades")]
    public List<GameObject> habilidades;

    [Header("Monedas")]
    public TextMeshProUGUI monedasTMP;

    [Header("Vida")]
    public Slider barraVida;

    [Header("Enemigos")]
    [SerializeField] public TextMeshProUGUI enemigosRestantesTMP;
    public Slider barraVidaJefe;

    [Header("Pause Menu")]
    [SerializeField] public Canvas pauseMenu;

    [Header("Shop Menu")]
    [SerializeField] public Canvas shopMenu;

    [Header("Interactuable")]
    public GameObject interactuar;
    #endregion

    void Awake()
    {
        CrearSingleton();
    }

    void Start()
    {
        barraVida.maxValue = GameManager.i.vidaMax;
        barraVida.value = GameManager.i.vidaMax;
        CargarHabilidades();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void CargarHabilidades()
    {
        for (int i = 0; i < habilidades.Count; i++)
        {
            Image skillImg = habilidades[i].transform.Find("Imagen").transform.GetComponent<Image>();
            Image keyImg = habilidades[i].transform.Find("Imagen Tecla").transform.GetComponent<Image>();
            Image skillImgCooldown = habilidades[i].transform.Find("Imagen Cooldown").transform.GetComponent<Image>();
            Text skillText = habilidades[i].transform.Find("Texto").transform.GetComponent<Text>();

            skillImg.sprite = GameManager.i.habilidadesActuales[i].SkillImg;
            keyImg.sprite = GameManager.i.habilidadesActuales[i].KeyImg;
            skillImgCooldown.sprite = GameManager.i.habilidadesActuales[i].SkillImg;
            skillImgCooldown.transform.GetComponent<Image>().fillAmount = 0f;
            skillText.text = "";
            if (GameManager.i.habilidadesActuales[i].name == "Nada")
            {
                habilidades[i].SetActive(false);
            }
            else
            {
                habilidades[i].SetActive(true);
            }

            if (GameManager.i.habilidadesActuales[i].name == "Rayo")
            {
                habilidades[i].transform.Find("Barra").gameObject.SetActive(true);
                habilidades[i].GetComponentInChildren<Slider>(true).maxValue = GameManager.i.habilidadesActuales[1].duracion;
                habilidades[i].GetComponentInChildren<Slider>(true).value = GameManager.i.habilidadesActuales[1].duracion;
            }
            else
            {
                habilidades[i].transform.Find("Barra").gameObject.SetActive(false);
            }
        }
    }

    public void CambiarFillAmountHabilidad(int habilidad, int fillAmount)
    {
        Image skillImgCooldown = habilidades[habilidad].transform.Find("Imagen Cooldown").transform.GetComponent<Image>();
        skillImgCooldown.transform.GetComponent<Image>().fillAmount = fillAmount;
    }

    public void CambiarVida(Slider barraVida, float vida)
    {
        barraVida.value = vida;
    }

    public void CambiarVidaJefe(float vida)
    {
        barraVidaJefe.value = vida;
    }

    public void HabilidadCooldown(float tiempo, float tiempoCooldown, int skill, bool continuar)
    {
        Image skillImgCooldown = habilidades[skill].transform.Find("Imagen Cooldown").transform.GetComponent<Image>();
        Text skillText = habilidades[skill].transform.Find("Texto").transform.GetComponent<Text>();

        if (continuar)
        {
            if (tiempoCooldown <= 0)
            {
                if (skillImgCooldown != null)
                {
                    skillImgCooldown.fillAmount = 0f;
                }
                if (skillText != null)
                {
                    skillText.text = "";
                }
            }
            else
            {
                if (skillImgCooldown != null)
                {
                    skillImgCooldown.fillAmount = tiempoCooldown / tiempo;
                }
                if (skillText != null)
                {
                    if (tiempoCooldown < 1)
                    {
                        skillText.text = Math.Round(tiempoCooldown, 1).ToString();
                    }
                    else
                    {
                        skillText.text = Math.Round(tiempoCooldown, 0).ToString();
                    }
                }
            }
        }
    }

    public void CambiarTMP(TextMeshProUGUI TMP, string texto)
    {
        TMP.text = texto;
    }

    public void CambiarCantidadEnemigos()
    {
        enemigosRestantesTMP.text = "Enemies Left: " + GameManager.i.enemigosRestantes.ToString();
    }

}
