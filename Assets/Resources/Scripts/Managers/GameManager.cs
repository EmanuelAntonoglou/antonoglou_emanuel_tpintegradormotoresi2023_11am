using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEditor;
using Unity.VisualScripting;
using System.Linq;

public class GameManager : MonoBehaviour
{
    #region Propiedades
    [NonSerialized] public static GameManager i;

    [Header("Juego")]
    public bool reiniciar = false;
    public bool pauso = true;

    [Header("Habitaciones")]
    public List<ControlHabitacion> habitaciones;
    public ControlHabitacion habitacionActual;
    public Light luz;
    public Color lucesPrendidas;
    public Color lucesApagadas;
    public Color lucesJefe;
    public bool habitacionGanada = false;
    public GameObject tornado;
    [NonSerialized] public int enemigosRestantes;
    private GameObject nuevoTornado;
    Vector3 offsetCentroHabitacion = new Vector3(0f, 0f, 139f);

    [Header("Personaje")]
    public ControlPersonaje personaje;
    public List<Habilidad> habilidadesActuales;
    public List<Habilidad> habilidadesAnteriores;
    public int monedas = 0;

    [Header("Vida")]
    public float vidaMax;
    public float tiempoInvulnerabilidad;
    private float vida;
    private bool invulnerable;
    #endregion

    private void Awake()
    {
        CrearSingleton();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        invulnerable = false;
        vida = vidaMax;
        habitacionActual.estaActvo = true;
        habitacionActual.Inicializar();
        habilidadesAnteriores = habilidadesActuales.ToList();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pauso();
        }
        GanoHabitacion();
        if (vida == 0 || Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarNivel();
        }
        if (habitacionActual != null)
        {
            if (habitacionActual.name == "Descanso")
            {
                UIManager.i.enemigosRestantesTMP.enabled = false;
            }
            if (habitacionActual.name == "Jefe" && habitacionActual.spawn.Count > 0)
            {
                luz.color = lucesJefe;
                UIManager.i.barraVidaJefe.transform.parent.gameObject.SetActive(true);
                UIManager.i.CambiarVidaJefe(50);
            }
            if (habitacionActual.arena)
            {
                habitacionActual.arena.InicializarTecho();
            }
        }
        if (enemigosRestantes == 0)
        {
            for (int i = 1; i <= 7; i++)
            {
                if (Input.GetKeyDown(KeyCode.Alpha0 + i))
                {
                    CambiarSalaManualmente(i);
                }
            }
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        if (reiniciar)
        {
            ReiniciarNivel();
        }
    }

    public void Pauso()
    {
        if (!pauso)
        {
            AudioManager.i.PausarSonidosActivos(true);
            UIManager.i.pauseMenu.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
            pauso = true;
        }
        else
        {
            AudioManager.i.PausarSonidosActivos(false);
            UIManager.i.pauseMenu.gameObject.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
            pauso = false;
        }
    }

    public void ReiniciarNivel()
    {
        Time.timeScale = 1;
        Destroy(gameObject);
        Destroy(UIManager.i.gameObject);
        Destroy(AudioManager.i.gameObject);
        string nombreEscena = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(nombreEscena);
    }

    public void CambiarHabilidad(int habilidad, Habilidad habilidadRemplazar)
    {
        habilidadesActuales[habilidad].cooldownSkill.continuar = false;
        habilidadesActuales[habilidad] = habilidadRemplazar;
        habilidadesActuales[habilidad].cooldownSkill.continuar = true;
    }

    private void CambiarSalaManualmente(int numero)
    {
        if (habitaciones[numero - 1].spawn.Count != 0)
        {
            CambiarHabitacion(habitaciones[numero - 1]);
        }
        personaje.transform.position = habitaciones[numero - 1].transform.localPosition + offsetCentroHabitacion + new Vector3(0, 15, 0);
        habitacionGanada = false;
        Debug.Log("Cambiando a sala " + numero);
    }

    public void GanoHabitacion()
    {
        AudioSource[] audioSources = AudioManager.i.GetComponents<AudioSource>();
        AudioSource targetAudioSource = Array.Find(audioSources, source => source.clip.name == habitacionActual.cancion);

        if (targetAudioSource != null && targetAudioSource.isPlaying && targetAudioSource.clip.name == "Empire of Sand")
        {
            for (int i = 0; i < habitaciones.Count; i++)
            {
                habitaciones[i].AbrirPuertas();
            }
            habitacionGanada = true;
        }

        if (!habitacionGanada && enemigosRestantes == 0 && habitacionActual.spawn.Count == 0)
        {
            if (habitacionActual.tornado)
            {
                StartCoroutine(nuevoTornado.transform.Find("Collider").GetComponent<Tornado>().Aparecer(false));
            }
            if (!habitacionActual.luces)
            {
                habitacionActual.luces = true;
                ApagarLuces();
            }
            if (habitacionActual.arena != null)
            {
                habitacionActual.arena.estadoActual = LlenarArena.Estado.Bajando;
            }
            for (int i = 0; i < habitaciones.Count; i++)
            {
                habitaciones[i].AbrirPuertas();
            }
            habitacionGanada = true;
        }
    }

    private void AparecerTornado()
    {
        if (habitacionActual.tornado)
        {
            nuevoTornado = Instantiate(tornado, habitacionActual.transform.localPosition + offsetCentroHabitacion, Quaternion.identity);
            nuevoTornado.transform.Find("Collider").GetComponent<Tornado>().Inicializar();
            StartCoroutine(nuevoTornado.transform.Find("Collider").GetComponent<Tornado>().Aparecer(true));
        }
    }

    public void CambiarHabitacion(ControlHabitacion habitacion)
    {
        for (int i = 0; i < habitaciones.Count; i++)
        {
            habitaciones[i].CerrarPuertas();
        }
        habitacionActual = habitacion;
        habitacionActual.estaActvo = true;
        ApagarLuces();
        AparecerTornado();
        if (habitacionActual.cancion == "Empire of Sand" || habitacionActual.cancion == "Across Dimming Asterisms")
        {
            AudioSource[] audioSources = AudioManager.i.GetComponents<AudioSource>();
            foreach (AudioSource source in audioSources)
            {
                if (source.isPlaying)
                {
                    source.Pause();
                }
            }
        }

        habitacionActual.Inicializar();
    }

    public void ApagarLuces()
    {
        if (habitacionActual.luces)
        {
            luz.color = lucesPrendidas;
            luz.intensity = 0.5f;
        }
        else
        {
            luz.color = lucesApagadas;
            luz.intensity = 0.07f;
        }

        foreach (Transform hijo in habitacionActual.transform)
        {
            if (hijo.name == "Antorcha")
            {
                foreach (Transform hijoAntorcha in hijo)
                {
                    hijoAntorcha.gameObject.SetActive(habitacionActual.luces);
                }
            }
        }
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    
    public void CambiarVida(string estado, float cantidad)
    {
        if (estado == "Da�ar" && !invulnerable)
        {
            invulnerable = true;
            Invoke("TerminarInvulnerabilidad", tiempoInvulnerabilidad);
            AudioManager.i.ReproducirSonido("Hit");
            vida -= cantidad;
            if (vida < 0)
            {
                vida = 0;
            }
        }
        else if (estado == "Curar")
        {
            vida += cantidad;
            AudioManager.i.ReproducirSonido("Healing");
            if (vida > vidaMax)
            {
                vida = vidaMax;
            }
        }
        UIManager.i.CambiarVida(UIManager.i.barraVida, vida);
    }

    public void TerminarInvulnerabilidad()
    {
        invulnerable = false;
    }
}
