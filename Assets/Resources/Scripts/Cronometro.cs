using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Cronometro
{
    [NonSerialized] public float tiempo;
    [NonSerialized] public float tiempoRestante;
    [NonSerialized] public bool continuar = true;

    public Cronometro(float tiempo)
    {
        this.tiempo = tiempo;
    }

    public IEnumerator Tick(float valorCronometro, int decimales, bool mostrarEnUI, int habilidad)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante >= 0)
        {
            if (mostrarEnUI)
            {
                UIManager.i.HabilidadCooldown(tiempo, (float)Math.Round(tiempoRestante, decimales), habilidad, continuar);
            }
            yield return new WaitForSeconds(0.1f);
            tiempoRestante -= 0.1f;
        }
    }

    public bool Alarma()
    {
        if (tiempoRestante <= 0)
        {
            tiempoRestante = 0;
            return true;
        }
        return false;
    }

    public void Escribir()
    {
        UnityEngine.Debug.Log(Math.Round(tiempoRestante, 1));
    }
}
