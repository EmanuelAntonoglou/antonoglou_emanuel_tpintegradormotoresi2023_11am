using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desvanecer : MonoBehaviour
{
    [SerializeField] float tiempoDesvanecer = 1f;
    public bool termino = false;
    
    float senoMin = -1.1f;
    float senoMax = 0.51f;

    public IEnumerator Abrir(Material mat, GameObject objetoADesactivar)
    {
        float elapsedTime = 0f;

        while (elapsedTime < tiempoDesvanecer)
        {
            senoMin = Mathf.Lerp(-1.1f, senoMax, elapsedTime / tiempoDesvanecer);
            elapsedTime += Time.deltaTime;
            mat.SetFloat("_Desvanecer", senoMin);
            yield return null;
        }

        senoMin = senoMax;
        mat.SetFloat("_Desvanecer", senoMax);
        objetoADesactivar.GetComponent<BoxCollider>().enabled = false;
    }

    public IEnumerator Cerrar(Material mat, GameObject objetoADesactivar)
    {
        objetoADesactivar.GetComponent<BoxCollider>().enabled = true;
        float elapsedTime = 0f;

        while (elapsedTime < tiempoDesvanecer)
        {
            senoMin = Mathf.Lerp(senoMax, -1.1f, elapsedTime / tiempoDesvanecer);
            elapsedTime += Time.deltaTime;
            mat.SetFloat("_Desvanecer", senoMin);
            yield return null;
        }

        senoMin = -1f;
        mat.SetFloat("_Desvanecer", -1.1f);
    }
}
