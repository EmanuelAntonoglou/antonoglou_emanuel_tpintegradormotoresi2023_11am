using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    [System.Serializable]
    public class ObjetoComprar
    {
        public GameObject objeto;
        public int precio;
        public TipoObjeto tipoObjetoComprar;
        public bool comprado = false;
        public Button boton;
    }

    public List<ObjetoComprar> objetos = new List<ObjetoComprar>();
    public Tienda tiendaCanva;
    private bool objetoTienda = false;

    private void Start()
    {
        if (objetos.Count == 0)
        {
            objetoTienda = true;
        }
    }

    public enum TipoObjeto
    {
        Objeto,
        Arma,
        Habilidad,
    }

    private void Abrir()
    {
        UIManager.i.shopMenu.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameManager.i.pauso = true;
        Time.timeScale = 0;
        MostrarInterfaz();
    }

    private void MostrarInterfaz()
    {
        if (tiendaCanva.gameObject.activeSelf)
        {
            for (int i = 0; i < tiendaCanva.objetos.Count; i++)
            {
                tiendaCanva.objetos[i].boton = tiendaCanva.gameObject.transform.Find(i.ToString()).GetComponent<Button>();
            }
        }
        for (int i = 0; i < tiendaCanva.objetos.Count; i++)
        {
            if (tiendaCanva.objetos[i].precio == 0)
            {
                tiendaCanva.objetos[i].boton.gameObject.transform.Find("Precio").GetComponent<TextMeshProUGUI>().text = "   Free!";
            }
            else
            {
                tiendaCanva.objetos[i].boton.gameObject.transform.Find("Precio").GetComponent<TextMeshProUGUI>().text = tiendaCanva.objetos[i].precio.ToString();
            }
        
        }
    }

    public void OnBuyButton(int boton)
    {
        if (GameManager.i.monedas >= objetos[boton].precio & !objetoTienda)
        {
            if (objetos[boton].tipoObjetoComprar == TipoObjeto.Arma && !objetos[boton].comprado)
            {
                objetos[boton].comprado = true;
                objetos[boton].boton.interactable = false;
                Arma arma = objetos[boton].objeto.GetComponent<ControlDisparo>().arma;
                GameManager.i.personaje.transform.Find("Main Camera").Find("Arma").gameObject.GetComponent<ControlDisparo>().CambiarArma(arma, true);
            }
            else if (objetos[boton].tipoObjetoComprar == TipoObjeto.Habilidad && !objetos[boton].comprado)
            {
                if (objetos[boton].objeto.name == "ObjetoRayo")
                {
                    objetos[boton].comprado = true;
                    objetos[boton].boton.interactable = false;
                    Habilidad habilidad = objetos[boton].objeto.GetComponent<ObjetoHabilidad>().habilidad;
                    GameManager.i.CambiarHabilidad(1, habilidad);
                    GameManager.i.personaje.poder = GameManager.i.habilidadesActuales[1].duracion;
                    GameManager.i.personaje.rayo = GameManager.i.habilidadesActuales[1];
                    UIManager.i.CargarHabilidades();
                }
            }
            else if (objetos[boton].tipoObjetoComprar == TipoObjeto.Objeto)
            {
                if (objetos[boton].objeto.gameObject.CompareTag("Healing"))
                {
                    GameManager.i.CambiarVida("Curar", objetos[boton].objeto.gameObject.GetComponent<Healing>().healing);
                }
            }
            GameManager.i.monedas -= objetos[boton].precio;
            UIManager.i.CambiarTMP(UIManager.i.monedasTMP, GameManager.i.monedas.ToString());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador") && !GameManager.i.personaje.alfombra.GetComponent<ControlAlfombra>().activo)
        {
            UIManager.i.interactuar.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F))
            {
                Abrir();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            UIManager.i.interactuar.SetActive(false);
        }
    }

}
