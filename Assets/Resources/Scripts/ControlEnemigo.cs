using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlEnemigo : MonoBehaviour
{
    #region Propiedades
    [Header("Stats")]
    public float velMov;
    public float velRot;
    public float tiempoSalida;
    public float daņo;
    public float vida;
    public float intervaloDaņo = 0.05f;
    private float tiempoUltimoDaņo = 0f;
    [NonSerialized] public bool salio = false;

    [Header("Canvas Vida")]
    public GameObject canvaVidaPrefab;
    [SerializeField] float offsetY = 5f;
    private GameObject canvasInstanciado;
    [NonSerialized] public Slider barraVida;

    [Header("Drop")]
    [SerializeField] private GameObject moneda;

    [Header("Serpiente")]
    private ControlSerpiente controlSerpiente;

    [Header("Referencias")]
    [NonSerialized] public Salida salida;
    private GameObject Personaje;
    #endregion

    private void Start()
    {
        Personaje = GameManager.i.personaje.gameObject;
        salida = GetComponent<Salida>();
        controlSerpiente = GetComponent<ControlSerpiente>();
        if (canvaVidaPrefab != null)
        {
            canvasInstanciado = Instantiate(canvaVidaPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.Euler(0f, 180f, 0f));
            canvasInstanciado.SetActive(false);
            canvasInstanciado.GetComponent<BarraVida>().PasarValores(transform, offsetY);
            barraVida = canvasInstanciado.GetComponentInChildren<Slider>();
            barraVida.maxValue = vida;
            UIManager.i.CambiarVida(barraVida, vida);
        }
        salida.salir = new Cronometro(salida.tiempoSalida);
        StartCoroutine(salida.salir.Tick(tiempoSalida, 0, false, -1));
    }

    void Update()
    {
        if (controlSerpiente == null)
        {
            if (!salida.salir.Alarma())
            {
                MoverAdelante(puerta);
            }
            else
            {
                if (canvaVidaPrefab != null)
                {
                    canvasInstanciado.SetActive(true);
                }
                salio = true;
                MoverAJugador();
            }
        }
    }
    Transform puerta;

    public void MoverAdelante(Transform puerta)
    {
        Vector3 direccionHaciaJugador = GameManager.i.personaje.transform.position - transform.position;
        direccionHaciaJugador.y = 0f;
        direccionHaciaJugador.Normalize();

        transform.Translate(direccionHaciaJugador * velMov * Time.deltaTime, Space.World);
    }

    private void MoverAJugador()
    {
        Vector3 direccionJugador = (Personaje.transform.position - transform.position).normalized;
        Quaternion rotacionDeseada = Quaternion.LookRotation(direccionJugador);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotacionDeseada, velRot * Time.deltaTime);
        transform.Translate(velMov * Vector3.forward * Time.deltaTime);
    }

    public void RecibirDaņo(float daņo)
    {
        if (salio)
        {
            vida -= daņo;
            if (canvaVidaPrefab != null)
            {
                UIManager.i.CambiarVida(barraVida, vida);
            }
            else
            {
                UIManager.i.CambiarVidaJefe(vida);
            }
            if (vida <= 0)
            {
                Destroy(canvasInstanciado);
                Destroy(gameObject);
                GameManager.i.enemigosRestantes--;
                UIManager.i.CambiarCantidadEnemigos();
                UIManager.i.barraVidaJefe.transform.parent.gameObject.SetActive(false);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PuertaCerrada") ||
            collision.gameObject.CompareTag("Healing") ||
            collision.gameObject.CompareTag("Arma"))
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
        }
        if (collision.gameObject.CompareTag("Bala") && collision.gameObject.name != "Rayo Alargar")
        {
            RecibirDaņo(collision.gameObject.GetComponent<Bala>().daņo);
            UIManager.i.CambiarVida(UIManager.i.barraVidaJefe, vida);
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            float tiempoActual = Time.time;

            if (other.gameObject.name == "Rayo Alargar" && tiempoActual - tiempoUltimoDaņo >= intervaloDaņo)
            {
                RecibirDaņo(other.gameObject.GetComponent<Bala>().daņo);
                UIManager.i.CambiarVida(UIManager.i.barraVidaJefe, vida);
                tiempoUltimoDaņo = tiempoActual;
            }
        }
    }


    private void OnDestroy()
    {
        Instantiate(moneda, gameObject.transform.position, moneda.transform.rotation);
    }

}
