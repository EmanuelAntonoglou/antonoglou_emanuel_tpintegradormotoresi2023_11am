using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : MonoBehaviour
{
    #region Propiedades
    [Header("Movimiento")]
    [SerializeField] private float radioMovimiento = 40f;
    [SerializeField] private float velocidadMovimiento = 5f;
    [SerializeField] private float velocidadRotacion = 100f;
    private Vector3 destino;
    private Vector3 posicionOriginal;

    [Header("Expulsar")]
    [SerializeField] private float tiempoExpulsion = 5f;
    [SerializeField] private float fuerzaExpulsar = 2100;
    [SerializeField] private float resistencia = 20f;

    [Header("Atraccion")]
    [SerializeField] private float radioDeAtraccion = 230;
    [SerializeField] private float fuerzaAtraccion = 15f;
    [SerializeField] private List<string> tagsObjetosAtraibles = new List<string> { };
    private List<Rigidbody> objetosAtraidos = new List<Rigidbody>();

    [Header("Material")]
    [SerializeField] private float tiempoAnimacion = 3.5f;
    private Material material;
    private float dissolveAparecer = 0.804f;
    private float dissolveDesaparecer = 1f;

    [Header("General")]
    private bool activo = false;
    #endregion

    private void Update()
    {
        if (activo)
        {
            Mover();
        }
    }

    public void Inicializar()
    {
        material = transform.parent.transform.Find("Mesh").gameObject.GetComponent<Renderer>().sharedMaterial;
        CambiarRadio();
        posicionOriginal = transform.parent.position;
    }

    private void CambiarRadio()
    {
        Vector3 nuevoTamaño = new Vector3(radioDeAtraccion, transform.localScale.y, radioDeAtraccion);
        transform.localScale = nuevoTamaño;
    }

    public IEnumerator Aparecer(bool aparecer)
    {
        float tiempoPasado = 0f;
        float valorDistorsion;

        while (tiempoPasado < tiempoAnimacion)
        {
            float progreso = tiempoPasado / tiempoAnimacion;
            if (aparecer)
            {
                valorDistorsion = Mathf.Lerp(dissolveDesaparecer, dissolveAparecer, progreso);
            }
            else
            {
                activo = false;
                valorDistorsion = Mathf.Lerp(dissolveAparecer, dissolveDesaparecer, progreso);
            }
            material.SetFloat("_Dissolve", valorDistorsion);
            tiempoPasado += Time.deltaTime;

            yield return null;
        }

        if (aparecer)
        {
            material.SetFloat("_Dissolve", dissolveAparecer);
            activo = true;
            gameObject.SetActive(true);
            ElegirNuevoDestino();
            InvokeRepeating("ExpulsarObjetos", tiempoExpulsion, tiempoExpulsion);
        }
        else
        {
            material.SetFloat("_Dissolve", dissolveDesaparecer);
            Destroy(transform.parent.gameObject);
        }
    }

    private void Mover()
    {
        transform.parent.position = Vector3.MoveTowards(transform.parent.position, destino, velocidadMovimiento * Time.deltaTime);
        transform.parent.Rotate(Vector3.up, velocidadRotacion * Time.deltaTime);
        if (Vector3.Distance(transform.parent.position, destino) < 0.1f)
        {
            ElegirNuevoDestino();
        }
    }

    private void ElegirNuevoDestino()
    {
        float newX = posicionOriginal.x + Random.Range(-radioMovimiento, radioMovimiento);
        float newZ = posicionOriginal.z + Random.Range(-radioMovimiento, radioMovimiento);
        destino = new Vector3(newX, transform.parent.position.y, newZ);
    }

    private void AtraerObjeto(Rigidbody rbObjeto)
    {
        Vector3 direccionAlCentro = transform.position - rbObjeto.position;
        float escalaActual = Mathf.Clamp01(1f - direccionAlCentro.magnitude / radioDeAtraccion);
        transform.localScale = new Vector3(escalaActual * radioDeAtraccion, transform.localScale.y, escalaActual * radioDeAtraccion);
        if (escalaActual > 0)
        {
            rbObjeto.AddForce(direccionAlCentro * fuerzaAtraccion);
            rbObjeto.velocity *= 1f / (1f + resistencia * Time.deltaTime);
        }
    }

    private void ExpulsarObjetos()
    {
        foreach (Rigidbody rbObjeto in objetosAtraidos)
        {
            if (rbObjeto != null)
            {
                if (!rbObjeto.gameObject.CompareTag("Jugador"))
                {
                    Vector3 direccionExpulsion = (rbObjeto.transform.position - transform.position).normalized;
                    direccionExpulsion.y = 0;
                    rbObjeto.AddForce(direccionExpulsion * fuerzaAtraccion * fuerzaExpulsar);
                }
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (tagsObjetosAtraibles.Contains(other.tag) && activo)
        {
            AtraerObjeto(other.GetComponent<Rigidbody>());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (tagsObjetosAtraibles.Contains(other.tag) && activo)
        {
            Rigidbody rbObjeto = other.GetComponent<Rigidbody>();
            if (rbObjeto != null)
            {
                objetosAtraidos.Add(rbObjeto);
                AtraerObjeto(rbObjeto);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (tagsObjetosAtraibles.Contains(other.tag) && activo)
        {
            Rigidbody rbObjeto = other.GetComponent<Rigidbody>();
            if (rbObjeto != null)
            {
                objetosAtraidos.Remove(rbObjeto);
            }
        }
    }

}