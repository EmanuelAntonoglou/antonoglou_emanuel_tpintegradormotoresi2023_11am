using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LlenarArena : MonoBehaviour
{
    [SerializeField] private float velocidadSubida;
    [SerializeField] private GameObject techo;
    [NonSerialized] public Estado estadoActual;
    private Vector3 posicionOriginal;

    public enum Estado
    {
        Quieto,
        Subiendo,
        Bajando,
    }

    void Start()
    {
        posicionOriginal = transform.position;
    }

    void Update()
    {
        if (estadoActual == Estado.Subiendo)
        {
            Llenar();
        }
        else if (estadoActual == Estado.Bajando)
        {
            Vaciar();
        }
    }

    public void InicializarTecho()
    {
        techo = GameManager.i.habitacionActual.transform.Find("Techo").gameObject;
    }

    public void Llenar()
    {
        transform.Translate(Vector3.up * velocidadSubida * Time.deltaTime);
    }

    public void Vaciar()
    {
        if (estadoActual == Estado.Bajando)
        {
            transform.Translate(Vector3.down * velocidadSubida * 2 * Time.deltaTime);

            if (transform.position.y <= posicionOriginal.y)
            {
                techo.tag = "Untagged";
                transform.position = posicionOriginal;
            }
        }
    }
}
