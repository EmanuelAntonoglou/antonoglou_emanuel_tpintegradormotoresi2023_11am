using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatsEnemigo
{
    public float velMov;
    public float velRot;
    public float tiempoSalida;
    public float da�o;
    public float vida;
}
