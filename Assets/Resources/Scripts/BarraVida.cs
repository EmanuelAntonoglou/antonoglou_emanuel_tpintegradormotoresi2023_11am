using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraVida : MonoBehaviour
{
    [NonSerialized] public Transform enemigo;
    [NonSerialized] public float offsetY;

    void Update()
    {
        MoverCanvas();
    }

    public void PasarValores(Transform enemigo, float offsetY)
    {
        this.enemigo = enemigo;
        this.offsetY = offsetY;
    }

    public void MoverCanvas()
    {
        transform.LookAt(Camera.main.transform.position, Vector3.up);
        transform.rotation *= Quaternion.Euler(0f, 180f, 0f);
        if (enemigo != null)
        {
            transform.position = new Vector3(enemigo.position.x, enemigo.position.y + offsetY, enemigo.position.z);
        }
    }

}
