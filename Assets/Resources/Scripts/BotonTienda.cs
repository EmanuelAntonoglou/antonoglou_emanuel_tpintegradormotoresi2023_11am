using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonTienda : MonoBehaviour
{
    [NonSerialized] public bool presionado = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            presionado = true;
        }
    }

}
