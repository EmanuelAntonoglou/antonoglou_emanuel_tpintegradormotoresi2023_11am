using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TirarObjeto : MonoBehaviour
{
    public float altura;
    public float velocidad;

    private Rigidbody rb;
    Collider col;
    Collider meshCollider;

    private void Start()
    {
        Physics.defaultContactOffset = 0.5f;
    }

    public void LanzarParabola(Transform puerta)
    {
        transform.position = new Vector3(puerta.position.x, puerta.position.y - 2, puerta.position.z);
        DesactivarCollider();
        Invoke("ActivarCollider", 0.5f);
        rb = GetComponent<Rigidbody>();
        Vector3 direccionAlJugador = GameManager.i.personaje.transform.position - transform.position;
        direccionAlJugador.y = 0f;
        direccionAlJugador.Normalize();
        rb.velocity = direccionAlJugador * velocidad;
    }

    private void DesactivarCollider()
    {
        col = GetComponent<Collider>();
        meshCollider = GetComponent<MeshCollider>();
        col.enabled = false;
        if (meshCollider != null)
        {
            meshCollider.enabled = false;
        }
    }

    private void ActivarCollider()
    {
        col = GetComponent<Collider>();
        meshCollider = GetComponent<MeshCollider>();
        col.enabled = true;
        if (meshCollider != null)
        {
            meshCollider.enabled = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PuertaCerrada") ||
            collision.gameObject.CompareTag("Healing") ||
            collision.gameObject.CompareTag("Arma"))
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("PuertaCerrada") ||
            collision.gameObject.CompareTag("Healing") ||
            collision.gameObject.CompareTag("Arma"))
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
        }
    }

}
