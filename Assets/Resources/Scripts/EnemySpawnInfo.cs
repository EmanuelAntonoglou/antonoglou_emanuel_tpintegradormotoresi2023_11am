using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnInfo
{
    public GameObject enemyPrefab;
    public StatsEnemigo statsEnemigo;
    public ControlPuerta puerta;
    public float tiempoSpawn;
    public Cronometro cooldownSpawn;
}
