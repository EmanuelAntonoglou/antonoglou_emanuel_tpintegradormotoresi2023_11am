using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlHabitacion : MonoBehaviour
{
    public List<SpawnInfo> spawn;
    public string cancion;

    [Header("Eventos")]
    public LlenarArena arena;
    public bool luces = true;
    public bool tornado = false;

    [NonSerialized] public bool estaActvo = false;

    void Update()
    {
        if (estaActvo)
        {
            Spawnear();
            if (arena != null && arena.estadoActual != LlenarArena.Estado.Bajando)
            {
                arena.estadoActual = LlenarArena.Estado.Subiendo;
            }
        }
    }

    private void ReproducirSonido()
    {
        AudioManager.i.ReproducirSonido(cancion);
    }

    public void Inicializar()
    {
        if (cancion != "")
        {
            if (cancion == "Empire of Sand")
            {
                Invoke("ReproducirSonido", 0.3f);
            }
            else
            {
                ReproducirSonido();
            }
        }
        for (int i = 0; i < spawn.Count; i++)
        {
            ControlEnemigo controlEnemigos = spawn[i].enemyPrefab.GetComponent<ControlEnemigo>();

            if (controlEnemigos != null)
            {
                GameManager.i.enemigosRestantes++;
            }
        }
        UIManager.i.CambiarCantidadEnemigos();
        spawn[0].cooldownSpawn = new Cronometro(spawn[0].tiempoSpawn);
        StartCoroutine(spawn[0].cooldownSpawn.Tick(spawn[0].tiempoSpawn, 0, false, -1));
    }

    private void Spawnear()
    {
        if (spawn.Count != 0 && spawn[0].cooldownSpawn.Alarma())
        {
            spawn[0].puerta.CambiarColor(spawn[0].enemyPrefab);
            StartCoroutine(spawn[0].puerta.TitilarLuces(3, spawn[0].puerta.tiempoParaSpawnear / 6));
            StartCoroutine(spawn[0].puerta.SpawnEnemigo(spawn[0]));
            spawn.RemoveAt(0);
            if (spawn.Count != 0)
            {
                spawn[0].cooldownSpawn = new Cronometro(spawn[0].tiempoSpawn);
                StartCoroutine(spawn[0].cooldownSpawn.Tick(spawn[0].tiempoSpawn, 0, false, -1));
            }
        }
    }
    
    public void AbrirPuertas()
    {
        ControlPuerta[] hijosConControlPuerta = GetComponentsInChildren<ControlPuerta>();
        StopAllCoroutines();

        AudioManager.i.ReproducirSonido("Desvanecer");
        foreach (ControlPuerta hijo in hijosConControlPuerta)
        {
            Transform puertaCerrada = hijo.transform.Find("Puerta Cerrada");
            Desvanecer desvanecer = hijo.GetComponent<Desvanecer>();
            Material matPuertaCerrada = puertaCerrada.GetComponent<Renderer>().material;
            StartCoroutine(desvanecer.Abrir(matPuertaCerrada, puertaCerrada.gameObject));
        }
    }

    public void CerrarPuertas()
    {
        ControlPuerta[] hijosConControlPuerta = GetComponentsInChildren<ControlPuerta>();
        StopAllCoroutines();

        AudioManager.i.ReproducirSonido("Aparecer");
        foreach (ControlPuerta hijo in hijosConControlPuerta)
        {
            Transform puertaCerrada = hijo.transform.Find("Puerta Cerrada");
            Desvanecer desvanecer = hijo.GetComponent<Desvanecer>();
            Material matPuertaCerrada = puertaCerrada.GetComponent<Renderer>().material;
            StartCoroutine(desvanecer.Cerrar(matPuertaCerrada, puertaCerrada.gameObject));
        }
    }
}
