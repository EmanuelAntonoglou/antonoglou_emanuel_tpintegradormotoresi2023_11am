using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public float velocidadDash = 10f;
    public float tiempoEntreDash = 5f;
    private ControlEnemigo controlEnemigo;
    private bool invocoDash = false;

    private void Start()
    {
        controlEnemigo = GetComponent<ControlEnemigo>();
    }

    private void Update()
    {
        if (!invocoDash && controlEnemigo.salio)
        {
            InvokeRepeating("TPAleatorio", 0f, tiempoEntreDash);
            invocoDash = true;
        }
    }

    private void TPAleatorio()
    {
        Vector2 direccionAleatoria = Random.insideUnitCircle.normalized;
        TP(direccionAleatoria.y);
    }

    private void TP(float direccionY)
    {
        Vector3 posicionActual = transform.position;
        transform.position = posicionActual + new Vector3(0f, direccionY, 0f) * velocidadDash;
    }
}
