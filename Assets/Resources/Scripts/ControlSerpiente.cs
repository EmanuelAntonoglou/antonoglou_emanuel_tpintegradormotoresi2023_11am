using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSerpiente : MonoBehaviour
{
    [SerializeField] private float velocidadMovimiento;
    [SerializeField] private float distanciaMaximaColision = 15f;

    void Update()
    {
        MoverAdelante();
    }

    private void MoverAdelante()
    {
        Vector3 direccionHaciaAdelante = transform.forward;
        transform.Translate(direccionHaciaAdelante * velocidadMovimiento * Time.deltaTime, Space.World);
    }

    private void CambiarDireccion()
    {
        Vector3 nuevaDireccion = ObtenerDireccionSegura();
        transform.rotation = Quaternion.LookRotation(nuevaDireccion, Vector3.up);
    }

    private Vector3 ObtenerDireccionSegura()
    {
        Vector3 direccionIntento = Random.onUnitSphere;
        int maxIntentos = 100000;

        for (int i = 0; i < maxIntentos; i++)
        {
            direccionIntento.Normalize();
            if (!VerificarColisionPared(direccionIntento))
            {
                return direccionIntento;
            }
            direccionIntento = Random.onUnitSphere;
        }
        return direccionIntento;
    }

    private bool VerificarColisionPared(Vector3 direccion)
    {
        Ray rayo = new Ray(transform.position, direccion);
        RaycastHit hit;

        if (Physics.Raycast(rayo, out hit, distanciaMaximaColision))
        {
            int hitLayer = hit.collider.gameObject.layer;
            if (hitLayer == LayerMask.NameToLayer("Habitacion") || 
                hitLayer == LayerMask.NameToLayer("Suelo"))
            {
                return true;
            }
        }
        return false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Habitacion") ||
            collision.gameObject.layer == LayerMask.NameToLayer("Suelo"))
        {
            CambiarDireccion();
        }
    }

}
