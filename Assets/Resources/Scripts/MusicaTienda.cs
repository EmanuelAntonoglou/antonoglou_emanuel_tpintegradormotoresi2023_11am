using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MusicaTienda : MonoBehaviour
{
    private string cancion;
    private string cancionAnterior;
    [SerializeField] private string cancionEasterEgg;
    [SerializeField] private BotonTienda boton;
    [SerializeField] private Color color;
    [SerializeField] private Material matPared;
    [SerializeField] private Material matBlanco;
    [SerializeField] private GameObject pared;
    [SerializeField] private Animator animacionDoozy;
    [SerializeField] private Tienda tienda;
    private Color colorAnterior;
    public float velocidadTransicion = 1.0f;
    private float tiempoPasado;
    private bool entroHabitacion;


    private void Start()
    {
        cancion = gameObject.transform.parent.GetComponent<ControlHabitacion>().cancion;
        colorAnterior = RenderSettings.ambientLight;
    }

    private void Update()
    {
        if (entroHabitacion && boton.presionado)
        {
            CambiarColor();
        }
    }

    private void EntrarHabitacion()
    {
        RenderSettings.ambientLight = color;
        cancionAnterior = GameManager.i.habitacionActual.cancion;
        AudioManager.i.PausarSonido(cancionAnterior);
        AudioManager.i.ReproducirSonido(cancion);
    }

    private void SalirHabitacion()
    {
        pared.GetComponent<Renderer>().material = matPared;
        pared.GetComponent<VideoPlayer>().enabled = false;
        RenderSettings.ambientLight = colorAnterior;
        AudioManager.i.PausarSonido(cancion);
        AudioManager.i.ReproducirSonido(cancionAnterior);
    }

    private void CambiarColor()
    {
        pared.GetComponent<Renderer>().material = matBlanco;
        pared.GetComponent<VideoPlayer>().enabled = true;
        animacionDoozy.SetBool("BotonPresionado", true);
        for (int i = 0; i < tienda.objetos.Count; i++)
        {
            tienda.objetos[i].precio = 0;
        }
        tiempoPasado += Time.deltaTime;
        float t = tiempoPasado / velocidadTransicion;
        float nuevoTono = Mathf.Lerp(0f, 1f, t);
        RenderSettings.ambientLight = Color.HSVToRGB(nuevoTono, 1f, 1f);
        if (tiempoPasado >= velocidadTransicion)
        {
            tiempoPasado = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            EntrarHabitacion();
            entroHabitacion = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            entroHabitacion = false;
            SalirHabitacion();
        }
    }

}
