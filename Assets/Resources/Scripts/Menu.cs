using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public void OnPlayButton()
    {
        if (GameManager.i != null)
        {
            GameManager.i.reiniciar = true;
            GameManager.i.ReiniciarNivel();
        }
        SceneManager.LoadScene("Nivel1");
    }

    public void OnQuitButton()
    {
        Application.Quit();
    }

    public void OnResumeButton()
    {
        GameManager.i.Pauso();
    }

    public void OnCloseButton()
    {
        gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        GameManager.i.pauso = false;
        Time.timeScale = 1;
    }

    public void OnRestartButton()
    {
        GameManager.i.ReiniciarNivel();
    }

    public void OnMainMenuButton()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
