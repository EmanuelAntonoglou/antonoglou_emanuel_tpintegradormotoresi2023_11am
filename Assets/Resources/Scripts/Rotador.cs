using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    [SerializeField] private float velocidadRotacion = 30;
    [SerializeField] private Eje eje;

    public enum Eje
    {
        x,
        y,
        z
    }

    void Update()
    {
        Rotar();
    }

    private void Rotar()
    {
        if (eje == Eje.x)
        {
            transform.Rotate(new Vector3(velocidadRotacion, 0, 0) * Time.deltaTime);
        }
        else if (eje == Eje.y)
        {
            transform.Rotate(new Vector3(0, velocidadRotacion, 0) * Time.deltaTime);
        }
        else if (eje == Eje.z)
        {
            transform.Rotate(new Vector3(0, 0, velocidadRotacion) * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PuertaCerrada") ||
            collision.gameObject.CompareTag("Healing") ||
            collision.gameObject.CompareTag("Arma") || 
            collision.gameObject.CompareTag("Moneda"))
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
        }
    }
}
