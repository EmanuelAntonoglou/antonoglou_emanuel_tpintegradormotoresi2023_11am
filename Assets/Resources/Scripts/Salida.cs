using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salida : MonoBehaviour
{
    public float tiempoSalida;
    public Cronometro salir;
    
    void Start()
    {
        salir = new Cronometro(tiempoSalida);
        StartCoroutine(salir.Tick(tiempoSalida, 0, false, -1));
    }
}
