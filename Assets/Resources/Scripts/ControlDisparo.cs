using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlDisparo : MonoBehaviour
{
    public Arma arma;
    [SerializeField] private GameObject explosion;

    private ControlEnemigo enemigo;
    private static bool inicializoArma = false;

    private void Update()
    {
        if (!GameManager.i.pauso && ((arma.activa && Input.GetMouseButton(0) && !GameManager.i.personaje.rayoAlargando) || 
            enemigo != null && arma.tiempoUltimoDisparo + arma.delay < Time.time && enemigo.salida.salir.Alarma())) 
        {
            Disparar();
        }
    }

    private void Start()
    {
        enemigo = GetComponentInParent<ControlEnemigo>();
        if (enemigo != null)
        {
            arma = Instantiate(arma);
            CambiarArma(arma, true);
        }
    }

    public void CambiarArma(Arma armaActual, bool activar)
    {
        if (inicializoArma && enemigo == null)
        {
            AudioManager.i.ReproducirSonido("Equipar Arma");
        }
        else
        {
            inicializoArma = true;
        }
        arma = armaActual;
        arma.activa = activar;
        arma.origen = transform.Find("Origen").transform;
        arma.trailBala = arma.bala.GetComponent<TrailRenderer>();
    }

    public void Disparar()
    {
        TrailRenderer trail;
        if (arma.tiempoUltimoDisparo + arma.delay < Time.time)
        {
            AudioManager.i.ReproducirSonido("Disparo");

            Vector3 direccion = arma.origen.TransformDirection(Vector3.forward);

            if (Physics.Raycast(arma.origen.position, direccion, out RaycastHit hit, float.MaxValue, arma.capa))
            {
                string nombrePadre = transform.parent.name;
                if (nombrePadre == "Main Camera")
                {
                    trail = Instantiate(arma.trailBala, arma.origen.position, Quaternion.identity);
                }
                else
                {
                    trail = Instantiate(arma.trailBala, arma.origen.position, Quaternion.identity, this.transform);
                }
                GameObject nuevaExplosion = Instantiate(explosion, arma.origen.position, Quaternion.identity, this.transform);
                StartCoroutine(CrearTrailBala(trail, hit.point, hit.normal, true));
                arma.tiempoUltimoDisparo = Time.time;
            }
        }
    }

    private IEnumerator CrearTrailBala(TrailRenderer Trail, Vector3 HitPoint, Vector3 HitNormal, bool MadeImpact)
    {
        if (Trail == null)
        {
            yield break;
        }

        Vector3 startPosition = Trail.transform.position;
        float distance = Vector3.Distance(Trail.transform.position, HitPoint);
        float remainingDistance = distance;

        while (remainingDistance > 0)
        {
            if (Trail != null)
            {
                Trail.transform.position = Vector3.Lerp(startPosition, HitPoint, 1 - (remainingDistance / distance));
                remainingDistance -= arma.velocidad * Time.deltaTime;
            }
            else
            {
                yield break;
            }
            yield return null;
        }
        if (Trail != null)
        {
            Trail.transform.position = HitPoint;
            Destroy(Trail.gameObject, Trail.time);
        }
    }
}
