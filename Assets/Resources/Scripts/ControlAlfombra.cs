using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements.Experimental;
using System.Linq;

public class ControlAlfombra : MonoBehaviour
{
    [SerializeField] private Vector3 posicion;
    [SerializeField] private Habilidad habilidadDash;
    [SerializeField] private Habilidad habilidadEstacionar;
    [NonSerialized] public bool activo = false;
    [NonSerialized] public BoxCollider col;
    //[NonSerialized] private GameObject personaje;

    [SerializeField] private ControlCamara direccionCamara;

    private void Start()
    {
        col = GetComponent<BoxCollider>();
        habilidadDash.uso = false;
        //personaje = GameManager.i.personaje.gameObject;
    }

    private void Update()
    {
        if (activo)
        {
            Dash();
            GameManager.i.personaje.CargarRayo();
        }
    }

    private void FixedUpdate()
    {
        if (activo)
        {
            transform.localPosition = posicion;
            transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
        }
    }

    private void Subirse()
    {
        GameManager.i.habilidadesAnteriores = GameManager.i.habilidadesActuales.ToList();
        GameManager.i.personaje.SubirseAlfombra();
        GameManager.i.personaje.AcortarRayo();
        col.enabled = false;
        GameManager.i.CambiarHabilidad(1, habilidadDash);
        GameManager.i.CambiarHabilidad(2, habilidadEstacionar);
        UIManager.i.CargarHabilidades();
        Transform camara = GameManager.i.personaje.transform.Find("Main Camera").gameObject.transform;
        transform.SetParent(camara);
        Rigidbody rb = GameManager.i.personaje.GetComponent<Rigidbody>();
        rb.useGravity = false;
        activo = true;
        UIManager.i.interactuar.SetActive(false);
    }

    private void Dash()
    {
        Transform padreDelPadre = transform.parent.parent;
        Rigidbody rb = padreDelPadre.GetComponent<Rigidbody>();

        if (habilidadDash.duracionSkill.Alarma())
        {
            habilidadDash.uso = false;
            direccionCamara.activo = true;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            habilidadDash.duracionSkill.tiempoRestante = 0;
        }

        if (habilidadDash.cooldownSkill.Alarma())
        {
            habilidadDash.puedeUsar = true;
            StopAllCoroutines();
        }

        if (habilidadDash.puedeUsar && Input.GetKeyDown(KeyCode.LeftShift))
        {
            habilidadDash.uso = true;
            habilidadDash.puedeUsar = false;

            StartCoroutine(habilidadDash.duracionSkill.Tick(habilidadDash.duracion, 1, false, 1));
            StartCoroutine(habilidadDash.cooldownSkill.Tick(habilidadDash.cooldown, 1, true, 1));
            StartCoroutine(RotarPadre());

            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

            Vector3 direccionCamara = Camera.main.transform.forward;
            rb.AddForce(direccionCamara * habilidadDash.velocidad, ForceMode.Impulse);

            //StartCoroutine(DetenerDash(rb, habilidadDash.duracion));
        }
    }


    private IEnumerator DetenerDash(Rigidbody rb, float duracion)
    {
        yield return new WaitForSeconds(duracion);

        transform.parent.parent.rotation = Quaternion.identity;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    private IEnumerator RotarPadre()
    {
        direccionCamara.activo = false;
        float tiempoInicio = Time.time;
        Transform padre = transform.parent;

        float anguloTotalRotacion = 360f;
        float velocidadAngular = anguloTotalRotacion / habilidadDash.duracion;

        while (Time.time < tiempoInicio + habilidadDash.duracion)
        {
            float tiempoPasado = Time.time - tiempoInicio;
            float t = tiempoPasado / habilidadDash.duracion;

            float anguloRotacionPaso = velocidadAngular * Time.deltaTime;
            padre.Rotate(Vector3.forward, anguloRotacionPaso);

            yield return null;
        }
        padre.rotation = Quaternion.identity;
        yield return new WaitForSeconds(0.1f);
        padre.rotation = Quaternion.identity;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador") && !activo)
        {
            UIManager.i.interactuar.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F))
            {
                Subirse();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            UIManager.i.interactuar.SetActive(false);
        }
    }

}
