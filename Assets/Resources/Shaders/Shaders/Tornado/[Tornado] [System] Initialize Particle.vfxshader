//[Tornado] [System] Initialize Particle,0
//Don't delete the previous line or this one
#pragma kernel CSMain
#pragma only_renderers d3d11 playstation xboxone xboxseries vulkan metal switch opengl
#define NB_THREADS_PER_GROUP 64
#define HAS_VFX_ATTRIBUTES 1
#define VFX_PASSDEPTH_ACTUAL (0)
#define VFX_PASSDEPTH_MOTION_VECTOR (1)
#define VFX_PASSDEPTH_SELECTION (2)
#define VFX_PASSDEPTH_PICKING (3)
#define VFX_PASSDEPTH_SHADOW (4)
#define VFX_USE_LIFETIME_CURRENT 1
#define VFX_USE_ANGLEX_CURRENT 1
#define VFX_USE_ANGLEY_CURRENT 1
#define VFX_USE_ANGLEZ_CURRENT 1
#define VFX_USE_ALIVE_CURRENT 1
#define VFX_USE_AGE_CURRENT 1
#define VFX_STATIC_SOURCE_COUNT (1)
#define VFX_LOCAL_SPACE 1
#include "Packages/com.unity.render-pipelines.universal/Runtime/VFXGraph/Shaders/VFXDefines.hlsl"



CBUFFER_START(parameters)
    float3 Angle_b;
    float Lifetime_a;
CBUFFER_END


struct VFXAttributes
{
    float lifetime;
    float angleX;
    float angleY;
    float angleZ;
    bool alive;
    float age;
};

struct VFXSourceAttributes
{
};




#define USE_DEAD_LIST (VFX_USE_ALIVE_CURRENT && !HAS_STRIPS)

#define USE_PREFIX_SUM ((VFX_STATIC_SOURCE_COUNT > 1) || (VFX_USE_DYNAMIC_SOURCE_COUNT))

RWByteAddressBuffer attributeBuffer;
ByteAddressBuffer sourceAttributeBuffer;
#if USE_PREFIX_SUM
StructuredBuffer<uint> prefixSumSpawnCount;
#endif

CBUFFER_START(initParams)
#if !VFX_USE_SPAWNER_FROM_GPU
    uint nbSpawned;					// Numbers of particle spawned
    uint spawnIndex;				// Index of the first particle spawned
    uint dispatchWidth;
#if VFX_USE_DYNAMIC_SOURCE_COUNT
    uint nbEvents;					// Number of spawn events
#endif
#else
    uint offsetInAdditionalOutput;
	uint nbMax;
#endif
	uint systemSeed;
CBUFFER_END

#if USE_DEAD_LIST
RWStructuredBuffer<uint> deadListIn;
ByteAddressBuffer deadListCount; // This is bad to use a SRV to fetch deadList count but Unity API currently prevent from copying to CB
#endif

#if VFX_USE_SPAWNER_FROM_GPU
StructuredBuffer<uint> eventList;
ByteAddressBuffer inputAdditional;
#endif

#if HAS_STRIPS
RWStructuredBuffer<uint> stripDataBuffer;
#endif

#include "Packages/com.unity.visualeffectgraph/Shaders/Common/VFXCommonCompute.hlsl"
#include "Packages/com.unity.visualeffectgraph/Shaders/VFXCommon.hlsl"



void SetAttribute_F0142CB9(inout float lifetime, float Lifetime) /*attribute:lifetime Composition:Overwrite Source:Slot Random:Off channels:XYZ */
{
    lifetime = Lifetime;
}
void SetAttribute_48A7BEFF(inout float angleX, inout float angleY, inout float angleZ, float3 Angle) /*attribute:angle Composition:Overwrite Source:Slot Random:Off channels:XYZ */
{
    angleX = Angle.x;
    angleY = Angle.y;
    angleZ = Angle.z;
}



// Due to a bug in HLSL compiler, disable spurious "unitialized variable" due to mid function return statement
#pragma warning(push)
#pragma warning(disable : 4000)
#if HAS_STRIPS
bool GetParticleIndex(inout uint particleIndex, uint stripIndex)
{
	uint relativeIndex;
	InterlockedAdd(STRIP_DATA(STRIP_NEXT_INDEX, stripIndex), 1, relativeIndex);
	if (relativeIndex >= PARTICLE_PER_STRIP_COUNT) // strip is full
	{
		InterlockedAdd(STRIP_DATA(STRIP_NEXT_INDEX, stripIndex), -1); // Remove previous increment
		return false;
	}

	particleIndex = stripIndex * PARTICLE_PER_STRIP_COUNT + ((STRIP_DATA(STRIP_FIRST_INDEX, stripIndex) + relativeIndex) % PARTICLE_PER_STRIP_COUNT);
    return true;
}
#endif
#pragma warning(pop)

[numthreads(NB_THREADS_PER_GROUP,1,1)]
void CSMain(uint3 groupId          : SV_GroupID,
            uint3 groupThreadId    : SV_GroupThreadID)
{
    uint id = groupThreadId.x + groupId.x * NB_THREADS_PER_GROUP;
#if !VFX_USE_SPAWNER_FROM_GPU
    id += groupId.y * dispatchWidth * NB_THREADS_PER_GROUP;
#endif

#if VFX_USE_SPAWNER_FROM_GPU
    uint maxThreadId = inputAdditional.Load((offsetInAdditionalOutput * 2 + 0) << 2);
    uint currentSpawnIndex = inputAdditional.Load((offsetInAdditionalOutput * 2 + 1) << 2) - maxThreadId;
#else
    uint maxThreadId = nbSpawned;
    uint currentSpawnIndex = spawnIndex;
#endif

#if USE_DEAD_LIST
    maxThreadId = min(maxThreadId, deadListCount.Load(0x0));
#elif VFX_USE_SPAWNER_FROM_GPU
    maxThreadId = min(maxThreadId, nbMax); //otherwise, nbSpawned already clamped on CPU
#endif

    if (id < maxThreadId)
    {
#if VFX_USE_SPAWNER_FROM_GPU
        int sourceIndex = eventList[id];
#endif
		uint particleIndex = id + currentSpawnIndex;
		
#if !VFX_USE_SPAWNER_FROM_GPU
        int sourceIndex = 0;

        #if !VFX_USE_DYNAMIC_SOURCE_COUNT
            uint nbEvents = VFX_STATIC_SOURCE_COUNT;
        #endif

        #if USE_PREFIX_SUM
            uint left = 0;
            uint right = max(nbEvents, 1) - 1;
            while (left <= right)
            {
                sourceIndex = (int)((left + right) / 2);
                if (id.x < prefixSumSpawnCount[sourceIndex])
                {
                    if (sourceIndex == 0 || id.x >= prefixSumSpawnCount[max(sourceIndex - 1, 0)])
                        break;
                    right = sourceIndex - 1;
                }
                else
                {
                    left = sourceIndex + 1;
                }
            }
        #endif
#endif

		VFXAttributes attributes = (VFXAttributes)0;
		VFXSourceAttributes sourceAttributes = (VFXSourceAttributes)0;
		
        attributes.lifetime = (float)1;
        attributes.angleX = (float)0;
        attributes.angleY = (float)0;
        attributes.angleZ = (float)0;
        attributes.alive = (bool)true;
        attributes.age = (float)0;
        

#if VFX_USE_PARTICLEID_CURRENT
         attributes.particleId = particleIndex;
#endif
#if VFX_USE_SEED_CURRENT
        attributes.seed = WangHash(particleIndex ^ systemSeed);
#endif
#if VFX_USE_SPAWNINDEX_CURRENT
        attributes.spawnIndex = id;
#endif
#if HAS_STRIPS
#if !VFX_USE_SPAWNER_FROM_GPU
		
#else
        uint stripIndex = sourceIndex;
#endif
		stripIndex = min(stripIndex, STRIP_COUNT);

        if (!GetParticleIndex(particleIndex, stripIndex))
            return;

        const StripData stripData = GetStripDataFromStripIndex(stripIndex, PARTICLE_PER_STRIP_COUNT);
		InitStripAttributesWithSpawn(maxThreadId, particleIndex, attributes, stripData);
		// TODO Change seed to be sure we're deterministic on random with strip
#endif
        
        SetAttribute_F0142CB9( /*inout */attributes.lifetime, Lifetime_a);
        SetAttribute_48A7BEFF( /*inout */attributes.angleX,  /*inout */attributes.angleY,  /*inout */attributes.angleZ, Angle_b);
        

		
#if VFX_USE_ALIVE_CURRENT
        if (attributes.alive)
#endif       
        {
#if USE_DEAD_LIST
	        uint deadIndex = deadListIn.DecrementCounter();
            uint index = deadListIn[deadIndex];
#else
            uint index = particleIndex;
#endif
            attributeBuffer.Store((index * 0x1 + 0x0) << 2,asuint(attributes.lifetime));
            attributeBuffer.Store((index * 0x3 + 0x20) << 2,asuint(attributes.angleX));
            attributeBuffer.Store((index * 0x3 + 0x21) << 2,asuint(attributes.angleY));
            attributeBuffer.Store((index * 0x3 + 0x22) << 2,asuint(attributes.angleZ));
            attributeBuffer.Store((index * 0x1 + 0x80) << 2,uint(attributes.alive));
            attributeBuffer.Store((index * 0x1 + 0xA0) << 2,asuint(attributes.age));
            

        }
    }
}
