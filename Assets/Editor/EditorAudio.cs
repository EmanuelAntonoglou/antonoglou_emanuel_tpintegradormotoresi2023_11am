#if UNITY_EDITOR
using static UnityEngine.GraphicsBuffer;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AudioManager))]
class AudioManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var audioManager = (AudioManager)target;

        if (GUILayout.Button("Cargar Audios"))
        {
            audioManager.CargarAudios();
        }
        if (GUILayout.Button("Reiniciar Audios"))
        {
            audioManager.sonidos.Clear();
            audioManager.CargarAudios();
        }
    }
}
#endif